<?php

class Personne {
    //variable public//
    public float $taille;
    public float $poids;

    //fonctionnalité du construct//
    public function __construct(float $taille, float $poids){
        $this->taille = $taille;
        $this->poids = $poids;
    }

    //fonctionnalité GET//
    public function getTaille():float {
        return $this->taille;
    }
    public function getPoids():float {
        return $this->poids;
    }

    //fonctionnalité SET//
    public function setTaille(float $taille):void {
        $this->taille = $taille;
    }
    public function setPoids(float $poids):void {
        $this->poids = $poids;
    }


    //fonctionnalité IMC////////////////////////////////////////
    public function IMC(){
        $imc = $this->poids / ($this->taille * $this->taille);
        $msg = "Vous etes dans un état de ";
        if ($imc < 18) {
           echo "$msg maigreur morbide car";
        }
        else if ($imc>18.5) {
            echo "$msg maigreur car";
        }
        else if ($imc > 25) {
           echo "$msg obese car";
        }
        return $imc;
    }

}

//appel de l'objet Personne et appel de la fonctionnalité IMC 
$afficheIMC = new Personne(1.75, 70);
echo " votre IMC est de " . $afficheIMC->IMC();

?>